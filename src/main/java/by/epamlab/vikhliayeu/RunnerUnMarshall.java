package by.epamlab.vikhliayeu;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import by.epamlab.vikhliayeu.xjc.candys.Candys;

public class RunnerUnMarshall {

    private static final String MARSHALL_XML_RESOURCE =   "src/main/resources/candy_remarsh.xml";
    private static final String UNMARSHALL_XML_RESOURCE = "src/main/resources/candys.xml";

    public static void main(String[] args) {
        try {
            JAXBContext context = JAXBContext.newInstance(Candys.class);
            Unmarshaller u =  context.createUnmarshaller();
            FileReader fr = new FileReader(UNMARSHALL_XML_RESOURCE);
            Candys c = (Candys) u.unmarshal(fr);
            reMarsh(c);
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    
    private static void reMarsh(Candys c) {
        try {
            JAXBContext context = JAXBContext.newInstance(Candys.class);
            Marshaller m =  context.createMarshaller();
            m.marshal(c, new FileOutputStream(MARSHALL_XML_RESOURCE));
            m.marshal(c, System.out);
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}
