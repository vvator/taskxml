package by.epamlab.vikhliayeu.stax;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

public class EventFactory {

    private XMLStreamWriter xmlWriter;
    private static String SEPARATOR_LINE = System.getProperty("line.separator");

    interface IEvent {
        void process(XMLStreamReader xmlReader, XMLStreamWriter xmlWriter) throws XMLStreamException;
    }

    static IEvent factoryActionByEvent(int event) {
        switch (event) {
        case XMLStreamConstants.START_DOCUMENT:
            return docStart;
        case XMLStreamConstants.START_ELEMENT:
            return elStart;
        case XMLStreamConstants.CHARACTERS:
            return writerCharacters;
        case XMLStreamConstants.END_ELEMENT:
            return elEnd;
        case XMLStreamConstants.END_DOCUMENT:
            return docEnd;
        default:
            return notImplEvent;
        }
    }

    public static void process(XMLStreamReader xmlReader, XMLStreamWriter xmlWriter) throws XMLStreamException {        
        docStart.process(xmlReader, xmlWriter);
        xmlWriter.writeCharacters(SEPARATOR_LINE);
        while (xmlReader.hasNext()) {
            factoryActionByEvent(xmlReader.next()).process(xmlReader, xmlWriter);
        }
        docEnd.process(xmlReader, xmlWriter);
    }

    public XMLStreamWriter getXmlWriter() {
        return xmlWriter;
    }

    public void setXmlWriter(XMLStreamWriter xmlWriter) {
        this.xmlWriter = xmlWriter;
    }

    private static IEvent docStart = new IEvent() {
        public void process(XMLStreamReader xmlReader, XMLStreamWriter xmlWriter) throws XMLStreamException {
            xmlWriter.writeStartDocument(xmlReader.getCharacterEncodingScheme(), xmlReader.getVersion());
        }
    };

    private static IEvent elStart = new IEvent() {
        public void process(XMLStreamReader xmlReader, XMLStreamWriter xmlWriter) throws XMLStreamException {
            xmlWriter.writeStartElement(xmlReader.getLocalName());
            for (int i = 0; i < xmlReader.getAttributeCount(); ++i) {
                if (xmlReader.getLocalName().equals("water")
                        && xmlReader.getAttributeLocalName(i).equals("value")) {
                    xmlWriter.writeAttribute(xmlReader.getAttributeLocalName(i), "500");
                } else {
                    xmlWriter.writeAttribute(xmlReader.getAttributeLocalName(i),
                            xmlReader.getAttributeValue(i));
                }
            }
        }
    };

    private static IEvent writerCharacters = new IEvent() {
        public void process(XMLStreamReader xmlReader, XMLStreamWriter xmlWriter) throws XMLStreamException {
            xmlWriter.writeCharacters(xmlReader.getText());
        }
    };

    private static IEvent elEnd = new IEvent() {
        public void process(XMLStreamReader xmlReader, XMLStreamWriter xmlWriter) throws XMLStreamException {
            xmlWriter.writeEndElement();
        }
    };

    private static IEvent docEnd = new IEvent() {
        public void process(XMLStreamReader xmlReader, XMLStreamWriter xmlWriter) throws XMLStreamException {
            xmlWriter.writeEndDocument();
        }
    };

    private static IEvent notImplEvent = new IEvent() {
        public void process(XMLStreamReader xmlReader, XMLStreamWriter xmlWriter) throws XMLStreamException {
        }
    };
}
