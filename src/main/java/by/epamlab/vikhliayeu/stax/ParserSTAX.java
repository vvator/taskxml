package by.epamlab.vikhliayeu.stax;

import java.io.InputStream;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

public class ParserSTAX {

    private XMLStreamWriter xmlWriter;

    public XMLStreamWriter getXmlWriter() {
        return xmlWriter;
    }

    public void parse(InputStream inputStream) {
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        try {
            XMLStreamReader xmlReader = inputFactory.createXMLStreamReader(inputStream);
            EventFactory.process(xmlReader, xmlWriter);
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
    }

    public void setXmlWriter(XMLStreamWriter xmlWriter) {
        this.xmlWriter = xmlWriter;
    }

    public ParserSTAX(XMLStreamWriter xmlWriter) {
        this.xmlWriter = xmlWriter;
    }
}
