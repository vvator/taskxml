package by.epamlab.vikhliayeu;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;

import by.epamlab.vikhliayeu.stax.ParserSTAX;

public class RunnerStax {

    final static String PATH_IN_XML = "src/main/resources/candys.xml";
    final static String PATH_OUT_XML = "src/main/resources/candys_staxed.xml";
    final static String PATH_TO_XSD = "src/main/resources/candys.xsd";

    public static void main(String[] args) {
        System.out.println("start program");
        FileOutputStream fOut;
        FileInputStream fIn;

        try {
            System.out.println("File read: \"" + PATH_IN_XML + "\"");
            fIn = new FileInputStream(PATH_IN_XML);
            fOut = new FileOutputStream(PATH_OUT_XML);

            ParserSTAX parser = new ParserSTAX(XMLOutputFactory.newInstance().createXMLStreamWriter(fOut));
            parser.parse(fIn);
            System.out.println("File \"" + PATH_OUT_XML + "\" is created");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.err.println("Error file create" + PATH_OUT_XML);
        } catch (XMLStreamException e) {
            e.printStackTrace();
        } catch (FactoryConfigurationError e) {
            e.printStackTrace();
        }
        System.out.println("end program");
    }

}
