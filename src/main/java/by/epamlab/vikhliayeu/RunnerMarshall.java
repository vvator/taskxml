package by.epamlab.vikhliayeu;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.util.Random;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import by.epamlab.vikhliayeu.xjc.candys.CandyType;
import by.epamlab.vikhliayeu.xjc.candys.Candys;
import by.epamlab.vikhliayeu.xjc.candys.Candys.Candy;
import by.epamlab.vikhliayeu.xjc.candys.NutritionalValue;
import by.epamlab.vikhliayeu.xjc.candys.ObjectFactory;
import by.epamlab.vikhliayeu.xjc.ingredients.ChocolateCandyIngredients;
import by.epamlab.vikhliayeu.xjc.ingredients.ChocolateKind;
import by.epamlab.vikhliayeu.xjc.ingredients.MassValueMg;
import by.epamlab.vikhliayeu.xjc.mass.Unit;
import by.epamlab.vikhliayeu.xjc.mass.Value;

public class RunnerMarshall {

    private static class SampleIngredientsFactory extends by.epamlab.vikhliayeu.xjc.ingredients.ObjectFactory {
        private Random r = new Random();
        @Override
        public ChocolateCandyIngredients createChocolateCandyIngredients() {           
            ChocolateCandyIngredients i = new ChocolateCandyIngredients();
            i.setChocolateKind(ChocolateKind.MILK);
            i.setWater(new MassValueMg());
            i.getWater().setUnit(Unit.MG);
            i.getWater().setValue(new BigDecimal(r.nextInt(2500)));
            i.setSugar(new MassValueMg());
            i.getSugar().setUnit(Unit.MG);
            i.getSugar().setValue(new BigDecimal(r.nextInt(5000)));
            i.setVanilin(new MassValueMg());
            i.getVanilin().setUnit(Unit.MG);
            i.getVanilin().setValue(new BigDecimal(r.nextInt(100)));
            return i;
        }
    }

    private static class SampleCandyFactory extends ObjectFactory {
        private Random r = new Random();
        private by.epamlab.vikhliayeu.xjc.ingredients.ObjectFactory ingredientsFactory;

        public SampleCandyFactory(by.epamlab.vikhliayeu.xjc.ingredients.ObjectFactory ingredientsFactory) {
            this.ingredientsFactory = ingredientsFactory;
        }

        @Override
        public NutritionalValue createNutritionalValue() {
            NutritionalValue n = new NutritionalValue();
            n.setCarbohydrates(new Value());
            n.getCarbohydrates().setUnit(Unit.GR);
            n.getCarbohydrates().setValue(new BigDecimal(r.nextInt(100)));
            n.setFats(new Value());
            n.getFats().setUnit(Unit.GR);
            n.getFats().setValue(new BigDecimal(r.nextInt(100)));
            n.setProteins(new Value());
            n.getProteins().setUnit(Unit.GR);
            n.getProteins().setValue(new BigDecimal(r.nextInt(200)));
            return n;
        }

        public Candy newRamdomCandie(String nameCandy,String factory){
            Candy candy = new Candys.Candy();
            candy.setCalories(new BigDecimal(r.nextInt(1500)));
            candy.setIngredientsChocolate(ingredientsFactory.createChocolateCandyIngredients());
            candy.setName(nameCandy);
            candy.setNutritionalValue(this.createNutritionalValue());
            candy.setType(CandyType.CHOCOLATE);
            candy.setFactory(factory);
            return candy;            
        }
        
        @Override
        public Candys createCandys() {            
            Candys candys = new Candys();
            candys.getCandy().add(newRamdomCandie("NewYear","spartak"));
            candys.getCandy().add(newRamdomCandie("Assorti","kommunarka"));
            candys.getCandy().add(newRamdomCandie("SNIKERS","SNIKERS"));
            candys.getCandy().add(newRamdomCandie("Bounty","SNIKERS"));            
            return candys;
        }
    }

    public static void main(String[] args) {
        ObjectFactory factory = new SampleCandyFactory(new SampleIngredientsFactory());
        try {
            JAXBContext context = JAXBContext.newInstance(Candys.class);
            Marshaller m = context.createMarshaller();
            Candys c = factory.createCandys();           
            m.marshal(c, new FileOutputStream("src/main/resources/candys.xml"));
            m.marshal(c, System.out);
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
